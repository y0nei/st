# st - simple terminal

st is a simple terminal emulator for X which sucks less.

### Patches
Decided to swap from manually patching to [st-flexipatch](https://github.com/bakkeby/st-flexipatch) for easy managment.

This repo contains files that are stipped from the flexipatch code using [flexipatch-finalizer](https://github.com/bakkeby/flexipatch-finalizer).

The only patch i have applied manually is the `st-cyclefonts` patch since i can never decide what font to use lol.
### Requirements
In order to build st you need the Xlib header files.

### Customization
edit `config.def.h` to edit the default configuration or `config.h` and `make install`

### Installation
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st *(if
necessary as root)*: `make clean install`

### Running st
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

### Credits
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.
